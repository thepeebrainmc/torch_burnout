package com.gitlab.thepeebrain.torchburnout;

import com.gitlab.thepeebrain.torchburnout.world.GameRules;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroups;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.gitlab.thepeebrain.torchburnout.block.Blocks.EMPTY_LANTERN;

public class TorchBurnout
		implements ModInitializer
{
	public static final String MOD_ID = "torch_burnout";

	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	@Override
	public void onInitialize()
	{
		GameRules.init();

		Identifier id = new Identifier(MOD_ID, "empty_lantern");
		BlockItem blockItem = new BlockItem(EMPTY_LANTERN, new FabricItemSettings());

		Registry.register(Registries.BLOCK, id, EMPTY_LANTERN);
		Registry.register(Registries.ITEM, id, blockItem);

		ItemGroupEvents.modifyEntriesEvent(ItemGroups.FUNCTIONAL)
				.register(content -> content.addAfter(Items.SOUL_LANTERN, EMPTY_LANTERN));
	}
}
