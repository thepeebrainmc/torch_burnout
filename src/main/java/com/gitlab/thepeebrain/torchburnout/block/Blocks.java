package com.gitlab.thepeebrain.torchburnout.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.LanternBlock;
import net.minecraft.block.MapColor;
import net.minecraft.block.piston.PistonBehavior;
import net.minecraft.sound.BlockSoundGroup;

public class Blocks
{
	public static final Block EMPTY_LANTERN;

	static
	{
		EMPTY_LANTERN = new LanternBlock(
				AbstractBlock.Settings.create()
						.mapColor(MapColor.IRON_GRAY)
						.solid()
						.requiresTool()
						.strength(3.5F)
						.sounds(BlockSoundGroup.LANTERN)
						.luminance((state) -> 0)
						.nonOpaque()
						.pistonBehavior(PistonBehavior.DESTROY)
		);
	}
}
