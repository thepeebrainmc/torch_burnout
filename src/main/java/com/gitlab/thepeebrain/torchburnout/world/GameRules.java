package com.gitlab.thepeebrain.torchburnout.world;

import com.gitlab.thepeebrain.torchburnout.TorchBurnout;
import net.fabricmc.fabric.api.gamerule.v1.CustomGameRuleCategory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class GameRules
{
	public static final CustomGameRuleCategory TORCH_BURNOUT_CATEGORY;

	public static final net.minecraft.world.GameRules.Key<net.minecraft.world.GameRules.BooleanRule> DO_TORCH_DROPS;
	public static final net.minecraft.world.GameRules.Key<net.minecraft.world.GameRules.BooleanRule> DO_TORCH_BURNOUT;
	public static final net.minecraft.world.GameRules.Key<net.minecraft.world.GameRules.IntRule> MAX_CAMPFIRE_AGE;
	public static final net.minecraft.world.GameRules.Key<net.minecraft.world.GameRules.IntRule> MAX_CANDLE_AGE;
	public static final net.minecraft.world.GameRules.Key<net.minecraft.world.GameRules.IntRule> MAX_JACK_O_LANTERN_AGE;
	public static final net.minecraft.world.GameRules.Key<net.minecraft.world.GameRules.IntRule> MAX_LANTERN_AGE;
	public static final net.minecraft.world.GameRules.Key<net.minecraft.world.GameRules.IntRule> MAX_TORCH_AGE;

	public static void init() {}

	static
	{
		TORCH_BURNOUT_CATEGORY = new CustomGameRuleCategory(new Identifier(TorchBurnout.MOD_ID,
				"sleep_burnout_category"), Text.translatable("key.categories.torch_burnout"));

		DO_TORCH_DROPS = GameRuleRegistry.register("doTorchDrops", TORCH_BURNOUT_CATEGORY,
				GameRuleFactory.createBooleanRule(false));
		DO_TORCH_BURNOUT = GameRuleRegistry.register("doTorchBurnout", TORCH_BURNOUT_CATEGORY,
				GameRuleFactory.createBooleanRule(true));
		MAX_CAMPFIRE_AGE = GameRuleRegistry.register("maxCampfireAge", TORCH_BURNOUT_CATEGORY,
				GameRuleFactory.createIntRule(11, 0, 15));
		MAX_CANDLE_AGE = GameRuleRegistry.register("maxCandleAge", TORCH_BURNOUT_CATEGORY,
				GameRuleFactory.createIntRule(11, 0, 15));
		MAX_JACK_O_LANTERN_AGE = GameRuleRegistry.register("maxJackOLanternAge", TORCH_BURNOUT_CATEGORY,
				GameRuleFactory.createIntRule(7, 0, 15));
		MAX_LANTERN_AGE = GameRuleRegistry.register("maxLanternAge", TORCH_BURNOUT_CATEGORY,
				GameRuleFactory.createIntRule(7, 0, 15));
		MAX_TORCH_AGE = GameRuleRegistry.register("maxTorchAge", TORCH_BURNOUT_CATEGORY,
				GameRuleFactory.createIntRule(7, 0, 15));
	}
}
