package com.gitlab.thepeebrain.torchburnout;

import net.minecraft.block.BlockState;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;

import static com.gitlab.thepeebrain.torchburnout.world.GameRules.DO_TORCH_BURNOUT;
import static net.minecraft.block.Block.NOTIFY_ALL;
import static net.minecraft.state.property.Properties.AGE_15;

public interface Extinguishable
{
	BlockState getExtinguishedState(BlockState state);

	boolean hasRandomTicks(BlockState state);

	void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random);

	int getMaxTorchAge(World world);

	default void extinguish(BlockState state, ServerWorld world, BlockPos pos)
	{
		if(!world.getGameRules().getBoolean(DO_TORCH_BURNOUT))
		{
			return;
		}

		int age = state.get(AGE_15);
		if(age >= getMaxTorchAge(world))
		{
			world.setBlockState(pos, getExtinguishedState(state), NOTIFY_ALL);
		}
		else
		{
			world.setBlockState(pos, state.with(AGE_15, age + 1));
		}
	}
}
