package com.gitlab.thepeebrain.torchburnout.mixin;

import com.gitlab.thepeebrain.torchburnout.Extinguishable;
import net.minecraft.block.*;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static com.gitlab.thepeebrain.torchburnout.world.GameRules.MAX_CAMPFIRE_AGE;
import static net.minecraft.block.CampfireBlock.LIT;
import static net.minecraft.state.property.Properties.AGE_15;

@Mixin(CampfireBlock.class)
public abstract class CampfireBlockMixin
		extends BlockWithEntity
		implements Waterloggable, Extinguishable
{
	protected CampfireBlockMixin(Settings settings)
	{
		super(settings);
	}

	@Inject(at = @At("HEAD"), method = "appendProperties(Lnet/minecraft/state/StateManager$Builder;)V")
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder, CallbackInfo info)
	{
		builder.add(AGE_15);
	}

	// EXTINGUISHABLE =============================================================================================== //

	@Override
	public int getMaxTorchAge(World world)
	{
		return world.getGameRules().getInt(MAX_CAMPFIRE_AGE);
	}

	@Override
	public BlockState getExtinguishedState(BlockState state)
	{
		return state.with(LIT, false).with(AGE_15, 0);
	}

	@Override
	public boolean hasRandomTicks(BlockState state)
	{
		return state.get(LIT);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random)
	{
		super.randomTick(state, world, pos, random);

		extinguish(state, world, pos);
	}
}
