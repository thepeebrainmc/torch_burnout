package com.gitlab.thepeebrain.torchburnout.mixin;

import com.gitlab.thepeebrain.torchburnout.Extinguishable;
import com.gitlab.thepeebrain.torchburnout.Ignitable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.LanternBlock;
import net.minecraft.block.Waterloggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static com.gitlab.thepeebrain.torchburnout.block.Blocks.EMPTY_LANTERN;
import static com.gitlab.thepeebrain.torchburnout.world.GameRules.MAX_LANTERN_AGE;
import static net.minecraft.block.Blocks.LANTERN;
import static net.minecraft.block.Blocks.SOUL_LANTERN;
import static net.minecraft.block.Blocks.SOUL_TORCH;
import static net.minecraft.block.Blocks.TORCH;
import static net.minecraft.block.LanternBlock.HANGING;
import static net.minecraft.block.LanternBlock.WATERLOGGED;
import static net.minecraft.state.property.Properties.AGE_15;

@Mixin(LanternBlock.class)
public abstract class LanternBlockMixin
		extends Block
		implements Waterloggable, Extinguishable, Ignitable
{
	public LanternBlockMixin(Settings settings)
	{
		super(settings);
	}

	@Inject(at = @At("HEAD"), method = "appendProperties(Lnet/minecraft/state/StateManager$Builder;)V")
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder, CallbackInfo info)
	{
		builder.add(AGE_15);
	}

	// EXTINGUISHABLE =============================================================================================== //

	@Override
	public int getMaxTorchAge(World world)
	{
		return world.getGameRules().getInt(MAX_LANTERN_AGE);
	}

	@Override
	public BlockState getExtinguishedState(BlockState state)
	{
		return EMPTY_LANTERN.getDefaultState()
				.with(HANGING, state.get(HANGING))
				.with(WATERLOGGED, state.get(WATERLOGGED))
				.with(AGE_15, 0);
	}

	@Override
	public boolean hasRandomTicks(BlockState state)
	{
		return true;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random)
	{
		super.randomTick(state, world, pos, random);

		extinguish(state, world, pos);
	}

	// IGNITABLE ==================================================================================================== //

	@Override
	public boolean isInExtinguishedState(BlockState state)
	{
		return state.isOf(EMPTY_LANTERN);
	}

	@Override
	public BlockState getIgnitedStateFor(BlockState state, ItemStack stackInHand)
	{
		if(stackInHand.getItem() instanceof BlockItem blockItem)
		{
			Block block = blockItem.getBlock();
			BlockState newState;
			if(block == TORCH)
			{
				newState = LANTERN.getDefaultState();
			}
			else if(block == SOUL_TORCH)
			{
				newState = SOUL_LANTERN.getDefaultState();
			}
			else
			{
				return null;
			}
			return newState.with(HANGING, state.get(HANGING))
					.with(WATERLOGGED, state.get(WATERLOGGED))
					.with(AGE_15, 0);
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockHitResult hit)
	{
		return ignite(state, world, pos, player, hand, hit);
	}
}
