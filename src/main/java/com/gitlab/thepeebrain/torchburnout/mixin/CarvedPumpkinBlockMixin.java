package com.gitlab.thepeebrain.torchburnout.mixin;

import com.gitlab.thepeebrain.torchburnout.Extinguishable;
import com.gitlab.thepeebrain.torchburnout.Ignitable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CarvedPumpkinBlock;
import net.minecraft.block.HorizontalFacingBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static com.gitlab.thepeebrain.torchburnout.world.GameRules.MAX_JACK_O_LANTERN_AGE;
import static net.minecraft.block.Blocks.CARVED_PUMPKIN;
import static net.minecraft.block.Blocks.JACK_O_LANTERN;
import static net.minecraft.block.Blocks.TORCH;
import static net.minecraft.state.property.Properties.AGE_15;

@Mixin(CarvedPumpkinBlock.class)
public abstract class CarvedPumpkinBlockMixin
		extends HorizontalFacingBlock
		implements Extinguishable, Ignitable
{
	public CarvedPumpkinBlockMixin(Settings settings)
	{
		super(settings);
	}

	@Inject(at = @At("HEAD"), method = "appendProperties(Lnet/minecraft/state/StateManager$Builder;)V")
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder, CallbackInfo info)
	{
		builder.add(AGE_15);
	}

	// EXTINGUISHABLE =============================================================================================== //

	@Override
	public int getMaxTorchAge(World world)
	{
		return world.getGameRules().getInt(MAX_JACK_O_LANTERN_AGE);
	}

	@Override
	public BlockState getExtinguishedState(BlockState state)
	{
		return CARVED_PUMPKIN.getDefaultState()
				.with(FACING, state.get(FACING))
				.with(AGE_15, 0);
	}

	@Override
	public boolean hasRandomTicks(BlockState state)
	{
		return state.isOf(JACK_O_LANTERN);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random)
	{
		super.randomTick(state, world, pos, random);

		extinguish(state, world, pos);
	}

	// IGNITABLE ==================================================================================================== //

	@Override
	public boolean isInExtinguishedState(BlockState state)
	{
		return state.isOf(CARVED_PUMPKIN);
	}

	@Override
	public BlockState getIgnitedStateFor(BlockState state, ItemStack stackInHand)
	{
		return stackInHand.getItem() instanceof BlockItem blockItem &&
				blockItem.getBlock() == TORCH ?
				JACK_O_LANTERN.getDefaultState().with(FACING, state.get(FACING)).with(AGE_15, 0) :
				null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockHitResult hit)
	{
		return ignite(state, world, pos, player, hand, hit);
	}
}
