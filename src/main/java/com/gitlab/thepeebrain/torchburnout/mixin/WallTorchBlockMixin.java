package com.gitlab.thepeebrain.torchburnout.mixin;

import net.minecraft.block.*;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.state.StateManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(WallTorchBlock.class)
public abstract class WallTorchBlockMixin
		extends TorchBlock
{
	public WallTorchBlockMixin(AbstractBlock.Settings settings, ParticleEffect particleEffect)
	{
		super(settings, particleEffect);
	}

	@Inject(at = @At("HEAD"), method = "appendProperties(Lnet/minecraft/state/StateManager$Builder;)V")
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder, CallbackInfo info)
	{
		super.appendProperties(builder);
	}
}
