package com.gitlab.thepeebrain.torchburnout.mixin;

import com.gitlab.thepeebrain.torchburnout.Disposable;
import com.gitlab.thepeebrain.torchburnout.Extinguishable;
import com.gitlab.thepeebrain.torchburnout.world.GameRules;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.TorchBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;

import static com.gitlab.thepeebrain.torchburnout.world.GameRules.MAX_TORCH_AGE;
import static net.minecraft.block.Blocks.AIR;
import static net.minecraft.state.property.Properties.AGE_15;

@Mixin(TorchBlock.class)
public abstract class TorchBlockMixin
		extends Block
		implements Extinguishable, Disposable
{
	public TorchBlockMixin(Settings settings)
	{
		super(settings);
	}

	@Override
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder)
	{
		super.appendProperties(builder);
		Identifier id = Registries.BLOCK.getId(this);
		if(!id.getPath().equals("redstone_torch") && !id.getPath().equals("redstone_wall_torch"))
		{
			builder.add(AGE_15);
		}
	}

	// EXTINGUISHABLE =============================================================================================== //

	@Override
	public int getMaxTorchAge(World world)
	{
		return isExtinguishableTorch() ? world.getGameRules().getInt(MAX_TORCH_AGE) : 0;
	}

	@Override
	public BlockState getExtinguishedState(BlockState state)
	{
		return isExtinguishableTorch() ? AIR.getDefaultState() : state;
	}

	@Override
	public boolean hasRandomTicks(BlockState state)
	{
		return isExtinguishableTorch();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random)
	{
		super.randomTick(state, world, pos, random);

		if(isExtinguishableTorch())
		{
			extinguish(state, world, pos);
		}
	}

	@Override
	public boolean shouldDropItemsOnExplosion(Explosion explosion)
	{
		return !isExtinguishableTorch() && super.shouldDropItemsOnExplosion(explosion);
	}

	@Override
	public void afterBreak(World world, PlayerEntity player, BlockPos pos, BlockState state,
			@Nullable BlockEntity blockEntity, ItemStack stack)
	{
		if(!isExtinguishableTorch() || world.getGameRules().getBoolean(GameRules.DO_TORCH_DROPS))
		{
			super.afterBreak(world, player, pos, state, blockEntity, stack);
		}
	}

	private boolean isExtinguishableTorch()
	{
		return this != Blocks.REDSTONE_TORCH && this != Blocks.REDSTONE_WALL_TORCH;
	}
}
