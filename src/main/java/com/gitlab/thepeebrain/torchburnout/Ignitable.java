package com.gitlab.thepeebrain.torchburnout;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import static net.minecraft.block.Block.NOTIFY_ALL;

public interface Ignitable
{
	ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit);

	boolean isInExtinguishedState(BlockState state);

	BlockState getIgnitedStateFor(BlockState state, ItemStack stackInHand);

	default ActionResult ignite(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockHitResult hit)
	{
		if(isInExtinguishedState(state))
		{
			ItemStack stackInHand = player.getStackInHand(hand);
			BlockState newState = getIgnitedStateFor(state, stackInHand);
			if(newState != null)
			{
				world.setBlockState(pos, newState, NOTIFY_ALL);
				if(!world.isClient)
				{
					if(!player.getAbilities().creativeMode)
					{
						stackInHand.decrement(1);
					}
					world.playSound(null, pos, SoundEvents.BLOCK_STONE_PLACE, SoundCategory.BLOCKS, 1, 1);
				}
				return ActionResult.success(world.isClient);
			}
		}

		return ActionResult.PASS;
	}
}
