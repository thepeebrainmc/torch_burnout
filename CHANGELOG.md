# Changelog

All notable changes to this project will be documented in this file.

## [1.2.0] - 2022-01-04

### Fixed

- Crash caused by attempting to age redstone torch

## [1.1.0] - 2022-12-13

### Added

- Game rule
  - `doTorchDrops` - Controls resource drops from torches

### Changed

- Separated max age game rules for blocks that burn out
  - `maxCampfireAge`
  - `maxCandleAge`
  - `maxJackOLanternAge`
  - `maxLanternAge`
  - `maxTorchAge`

## [1.0.0] - 2022-12-08

### Changed

- Minimum torch age to `0`
- Block id `lantern_empty` to `empty_lantern`

## [0.2.1] - 2022-12-07

### Fixed

- Ticking error caused by missing `WallTorchBlock` property

## [0.2.0] - 2022-12-07

### Added

- Two new game rules
  - `doTorchBurnout` - If enabled, torches will burn out over time
  - `maxTorchAge` - The required amount of random ticks before a torch burns out

## [0.1.0] - 2022-12-06

### Added

- Burnout functionality to:
  - `minecraft:torch`
  - `minecraft:soul_torch`
  - `minecraft:campfire`
  - `minecraft:soul_campfire`
  - `minecraft:jack_o_lantern`
  - `minecraft:lantern`
  - `minecraft:soul_lantern`
  - `minecraft:candle` and its dye variants
- `torch_burnout:lantern_empty` block to replace `minecraft:lantern` and `minecraft:soul_lantern` when burning out
- New `minecraft:lantern` and `minecraft:soul_lantern` shapeless recipes with `torch_burnout:lantern_empty` and its associated torch