# Torch Burnout

Makes torches, campfires, lanterns, and candles burn out and revert to its unlit state.

**Overview**

This mod makes vanilla Minecraft's basic lighting blocks temporary and some even break after use. It is designed to make the early game and dungeon exploring harder.

This makes lighting one's base that much more difficult so it is best to use this mod with other mods that provide mid-to-late game lighting alternatives. Darkness and dynamic lighting mods are recommended as well.

**Affected Blocks**

- Torch and Soul Torch
- Campfire and Soul Campfire
- Lantern and Soul Lantern
- Jack o' Lantern
- Candle (including dye variants)

**Burning Out**

Burning out happens in aging stages similar to crop growth and relies on random ticks. Each stage has no particular effect other than to bring the light block closer to burning out. Once the maximum stage is reached, it is replaced by its corresponding unlit block.

- Torches and Soul Torches will be consumed and replaced by air
- Campfires, Soul Campfires, Candles (including dye variants) revert to its respective vanilla unlit states
- Jack o' Lanterns are replaced by Carved Pumpkins
- Lanterns and Soul Lanterns are replaced by a new Empty Lantern block

Since aging relies on random ticks, each light may take more or less time than another to burn out.

**Reigniting**

- Torches and Soul Torches must be replaced with new ones
- Campfires, Soul Campfires, Candles (including dye variants) can be lit the same way as vanilla
- Carved Pumpkins can now be right-clicked with a Torch to create Jack o' Lanterns
- The new Empty Lantern can be right-clicked with a Torch or Soul Torch to create Lanterns and Soul Lanterns respectively

**Recipes**

Lanterns and Soul Lanterns have an additional recipe through shapeless-crafting its respective torch with an Empty Lantern.

Empty Lanterns cannot be crafted but can be picked up from the world after a Lantern or Soul Lantern burns out.

**Game Rules**

`doTorchBurnout`

- If true, burning out will be enabled

`doTorchDrops`

- Controls whether Torches and Soul Torches drop as items when broken

`maxCampfireAge`, `maxCandleAge`, `maxJackOLanternAge`, `maxLanternAge`, `maxTorchAge`

- Sets the amount of random ticks before burning out
- range is between `0` and `15`

**Installation**

[Download here](https://www.curseforge.com/minecraft/mc-mods/thepeebrain-torch-burnout/files)

**Mod Dependencies**

- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api "Fabric API")